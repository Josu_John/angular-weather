# Weather

## Setup dev environment

- Install (node.js)[https://nodejs.org/en/download/]
- Update the latest version of npm with `npm install npm -g`

Run the following commands:

```sh
npm install
```

## Run the app

To run the app directly from memory. This is the best option for development. It uses webpack-dev-server and browserSync has a proxy. Open two terminal windows and run the proxy and the webpack server. The script will open the app in the browser.

```sh
npm run watch:dev
```

## Unit tests
- To run unit tests a single time

  ```sh
  npm run test
  ```

