
import {module, bootstrap} from 'angular';
require('angular-material/angular-material.css')


 import "@uirouter/angularjs";
 import 'angular-animate';
 import 'angular-aria';
 import 'angular-material';

import './modules';
var constants = require('config/config.json')

module('root', [
    'ui.router',
    'ngMaterial',
    'root.modules'
]).constant('constants',constants);


bootstrap(document, ["root"], {});