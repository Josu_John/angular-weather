import {module} from 'angular';
import { UrlRouterProvider, StateProvider } from '@uirouter/angularjs';

module('root.modules', [
    'root.modules.weather'
])
.config(rootConfig);


function rootConfig($stateProvider: StateProvider, $urlRouterProvider:UrlRouterProvider) {
    $stateProvider
        .state('root', {
            url: '/',
            template: "<ui-view />"
        })


    $urlRouterProvider.when('', '/weather')
    $urlRouterProvider.when('/', '/weather')
}
