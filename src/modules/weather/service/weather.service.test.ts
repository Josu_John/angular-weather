import {mock} from 'angular';

import { WeatherServivce } from './weather.service';
import './';
describe('weather serice', () => {
    let weatherService :WeatherServivce;
    let http
    let $q: ng.IQService;
    let weather;
    let constants;

    beforeEach(() => {
        mock.module('root.modules.weather.service');

        weather = [
            {
                description: 'description1',
                date: '2017-10-29T15:00:00',
                icon: '1'
            },
            {
                description: 'description1',
                date: '2017-10-29T15:00:00',
                icon: '1'
            }];

        http = {
            get: () => { }
        };

        constants = {
            BackendUrl: 'backend/'
        };

        mock.module(function (_$provide_: any): void {
            _$provide_.value('$http', http);
            _$provide_.value('constants', constants);
        });
    })


    beforeEach(inject(function ($injector) {
        $q = $injector.get('$q');
        weatherService = $injector.get('weatherService');

        var defer = $q.defer()
        defer.resolve(weather)
        spyOn(http, 'get').and.returnValue(defer.promise)

    }))
    it('weather service get method calls http get', () => {
         //act
        weatherService.get();

         //assert
        expect(http.get).toHaveBeenCalled();
    })

    it('weather service get method calls http get with backend/forcasts', () => {
        //act
        weatherService.get();

         //assert
        expect(http.get).toHaveBeenCalledWith(constants.BackendUrl+'forcasts');
    })

    it('weather service get returns 2 items', () => {
        //act
        let result =weatherService.get();

        //assert
        result.then((items)=>{
            expect(items.length).toBe(2);
        })
    })


    it('weather service get method calls http get with params', () => {
        //act
        weatherService.get('uk','london');

         //assert
        expect(http.get).toHaveBeenCalledWith(constants.BackendUrl+'forcasts?country=uk&city=london');
    })
})