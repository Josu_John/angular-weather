
import 'angular-mocks';
import { Weather } from '../weather.model'
export class WeatherServivce {

    constructor(private $http, private constants) {
    }

    get(country?, city?) :Promise<Array<Weather>> {
        let url =this.constants.BackendUrl+'forcasts';
        if(country && city)
        {
            url +='?country='+country+'&city='+city;
        }
         return this.$http.get(url)
         .then((response)=> response.data)
    }

}

WeatherServivce.$inject = ['$http', 'constants'];
