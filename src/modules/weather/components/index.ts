import {module} from 'angular';


import {WeatherListComponent} from './weather-list';
import {WeatherComponent} from './weather';


module('root.modules.weather.components', [])
.component('weather', new WeatherComponent())
.component('weatherList', new WeatherListComponent());
