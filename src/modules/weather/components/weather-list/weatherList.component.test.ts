
import {mock }from 'angular';


import { Weather } from '../../weather.model'
import '../'

describe('weather-list component', () => {
    let weather: Array<Weather>;
    let scope;
    let template;
    beforeEach(() => {

        mock.module('root.modules.weather.components');
        weather = [
            {
                description: 'description1',
                date: '2017-10-29T15:00:00',
                icon: '1',
                city:'London'
            },
            {
                description: 'description1',
                date: '2017-10-29T15:00:00',
                icon: '1',
                city:'London'
            }]
    })

    beforeEach(()=>{
        inject(( _$compile_, _$rootScope_)=>{     
            scope= _$rootScope_.$new();
            template =  _$compile_('<weather-list weather="data"></weather-list>')(scope);
            })
    })

    it('2 forcasts are displayed in UI', () => {
        //arrange
        scope.data= weather;

        //act 
        scope.$apply();

        //assert 
        expect(template.find('weather').children().length).toBe(2);
    })
})