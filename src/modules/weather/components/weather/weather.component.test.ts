
import { mock } from 'angular';


import { Weather } from '../../weather.model'
import '../'

describe('weather component', () => {
    let weather: Weather;
    let scope;
    let template;
    beforeEach(() => {

        mock.module('root.modules.weather.components');
        weather =
            {
                description: 'description1',
                date: '2017-10-29T15:00:00',
                icon: '1',
                city:'London'
            };
    })

    beforeEach(() => {
        inject((_$compile_, _$rootScope_) => {
            scope = _$rootScope_.$new();
            template = _$compile_('<weather forcast="forcast"></weather-list>')(scope);
        })
    })

    it('all the field values are translated correctly to ui', () => {
        //arrange
        scope.forcast = weather;

        //act 
        scope.$apply();
       
        //assert
        expect(template.find('div').eq(1).text().trim()).toBe('London')
        expect(template.find('div').eq(2).text().trim()).toBe('description1')
        expect(template.find('div').eq(3).text().trim()).toBe('Sunday, October 29, 3:00, 2017')
    })
})