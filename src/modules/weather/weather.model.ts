export class Weather
{
    date: string;
    description :string;
    icon:string;
    city: string;
    constructor(init=null)
    {
        Object.assign(this, init);
    }
}