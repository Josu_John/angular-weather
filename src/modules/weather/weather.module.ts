import { module } from 'angular';
import { StateProvider, StateService, StateParams } from '@uirouter/angularjs';

import {WeatherController} from './weather.controller'
import {WeatherServivce} from './service/weather.service'

module('root.modules.weather', [
    'root.modules.weather.service',
    'root.modules.weather.components'
])
    .config(config)

function config($stateProvider: StateProvider) {

    $stateProvider
        .state('root.weather', {
            url: 'weather?country&city',
            abstract: false,
            template: require('./weather.template.html'),
            resolve: {
                forcasts :forcasts
            },
            controller: WeatherController,
            controllerAs:'weatherCntrl'
        })
}


function forcasts(weatherService :WeatherServivce,$stateParams: StateParams) : Promise<any> {
    return weatherService.get($stateParams.country,$stateParams.city).then((response) => {
      return   response;
    })
}
forcasts.$inject=['weatherService','$stateParams'];