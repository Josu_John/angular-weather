import 'babel-polyfill';


import 'angular';
import 'angular-mocks';
import 'angular-material';
import 'moment';
import 'angular-cache';
import 'file-saver';
import 'sinon';
import 'jasmine-sinon';
import './modules/';

var testsContext = require.context('./', true, /.test$/);
testsContext.keys().forEach(testsContext);
