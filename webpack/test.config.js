var webpack = require('./webpack.config')

var config =
    {
        openBrowser: false,
        watch: false,
        quiet: false,
        eslint: false,
        tslint: false,
        devServer: false,
        middleTier:
        {
            baseurl: "http://localhost:8882"
        },
        tracking: false,
        noParse:[]
    };
var webpackConfig = webpack(config);

module.exports = {
    module: webpackConfig.module,
    resolve: webpackConfig.resolve      
}