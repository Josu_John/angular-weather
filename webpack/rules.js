
function factory(config) {
  var rules = [
    {
      test: /\.js$/,
      loaders: ['ng-annotate-loader']
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: "imports-loader"
    },
    {
      test: /\.js$/,
      exclude: /node_modules/,
      use: "exports-loader"
    },
    {
      test: /\.(eot|woff|ttf|svg)$/,
      loaders: ["file-loader?name=[path][name].[ext]?[hash]"]
    },
    {
      test: /\.jpg$/,
      exclude: /node_modules/,
      use: 'file-loader?name=[path][name]-[hash].[ext]'
    },
    {
      test: /\.gif$/,
      loader: 'file-loader?name=[path][name]-[hash].[ext]'
    },
    {
      test: /\.png$/,
      use: 'url-loader?limit=10000&mimetype=image/png&name=[path][name]-[hash].[ext]'
    },
    {
      test: /.html$/,
      use: 'raw-loader'
    },
    {
      test: /\.(sass|scss|css)$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader',
      ]
    },
    {
      test: /\.ts(x?)$/,
      loader: 'ts-loader'
    }
  ];

  if (config.eslint) {
    rules.push({
      enforce: "pre",
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "eslint-loader",
      options: {
        emitError: true,
        useEslintrc: true
      }
    });
  }

  if (config.tslint) {
    rules.push({
      enforce: "pre",
      test: /\.ts$/,
      exclude: /node_modules/,
      loader: "tslint-loader",
      options: {
        emitError: false,
        tsConfigFile: 'tsconfig.json'
      }
    });
  }

  return rules;
}

module.exports = factory;
