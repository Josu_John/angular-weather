'use strict';

const copyWebpack= require('copy-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

var HtmlWebpackPlugin = require('html-webpack-plugin')
var browserSync = require('browser-sync-webpack-plugin');
var CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
var src = path.resolve(__dirname, '../src')

var rules = require('./rules.js')

function factory(config) {
  var webpack_config =
    {
      context: src,
      entry: {
        index: './index.ts'
      },
      devtool: "source-map",
      resolve: {
        modules: [
          path.resolve("./node_modules"), 
          src
        ],
        extensions: ['.ts', '.js', '.json', '.scss'],
        alias: {
          common: 'common',
          model: 'modules/root/model',
          services: 'modules/root/services'
        }
      },
      module: {
        rules: rules(config)
      },
      plugins: [
        new CaseSensitivePathsPlugin(),
        new HtmlWebpackPlugin({
          template: 'index.template.html',
          inject: 'body',
          hash: true
        })
      ]
    };

  if(config.devServer){
    webpack_config.devServer = {
        hot: true,
        contentBase: 'src',
        watch: config.watch,
        port: 3100,
        secure: false,
        quiet: false,
        noInfo: false,
        stats: {
          colors: true
        }
      }
    };

  if (config.openBrowser) {
    webpack_config.plugins.push(
      new browserSync({
        host: 'localhost',
        port: 8883,
        proxy: 'http://localhost:' + 3100,
      })
    );
    webpack_config.plugins.push(
        new webpack.HotModuleReplacementPlugin()
    );
  }



  webpack_config.output = {
    path: path.resolve(__dirname, '../' + 'bin'),
    filename: '[name].bundle.js',
  };

  return webpack_config
}

module.exports = factory
