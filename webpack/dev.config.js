var webpack = require('./webpack.config')

var config =
    {
        openBrowser: true,
        watch: true,
        quiet: false,
        eslint: false,
        tslint: true,
        devServer: true,
        middleTier:
        {
            baseurl: "http://localhost:8882"
        },
        tracking:false
    };

module.exports = webpack(config)